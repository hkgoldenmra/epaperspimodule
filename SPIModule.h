#ifndef SPIMODULE_H
#define SPIMODULE_H

#include <Arduino.h>

class SPIModule {
	private:
		byte ssPin;
		byte mosiPin;
		byte misoPin;
		byte sckPin;
		void declare(byte, byte, byte, byte);
	public:
		SPIModule();
		SPIModule(byte, byte, byte, byte);
		void initial();
		byte transfer(byte);
};

#endif