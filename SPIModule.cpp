#include "SPIModule.h"

void SPIModule::declare(byte ssPin, byte mosiPin, byte misoPin, byte sckPin) {
	this->ssPin = ssPin;
	this->mosiPin = mosiPin;
	this->misoPin = misoPin;
	this->sckPin = sckPin;
}

/**
 * Constructs SPIModule object using default SS, MOSI, MISO and SCK pins of Arduino settings.
 */
SPIModule::SPIModule() {
	this->declare(SS, MOSI, MISO, SCK);
}

/**
 * Constructs SPIModule object using specific SS, MOSI, MISO and SCK pins.
 * @param byte ssPin The specific SS pin.
 * @param byte mosiPin The specific MOSI pin.
 * @param byte misoPin The specific MISO pin.
 * @param byte sckPin The specific SCK pin.
 */
SPIModule::SPIModule(byte ssPin, byte mosiPin, byte misoPin, byte sckPin) {
	this->declare(ssPin, mosiPin, misoPin, sckPin);
}

/**
 * Initials the pins modes and signals.
 */
void SPIModule::initial() {
	pinMode(this->ssPin, OUTPUT);
	digitalWrite(this->ssPin, HIGH);
	pinMode(this->mosiPin, OUTPUT);
	pinMode(this->misoPin, INPUT);
	pinMode(this->sckPin, OUTPUT);
	digitalWrite(this->sckPin, HIGH);
}

/**
 * Returns the signal of data from MISO.
 * @param byte data The signal of data from MOSI.
 * @return byte The signal of data from MISO.
 */
byte SPIModule::transfer(byte data) {
	byte response = 0;
	digitalWrite(this->ssPin, LOW);
	for (byte i = 0x80; i > 0; i >>= 1) {
		digitalWrite(this->sckPin, LOW);
		digitalWrite(this->mosiPin, ((data & i) > 0) ? HIGH : LOW);
		digitalWrite(this->sckPin, HIGH);
		response |= ((digitalRead(this->misoPin) > LOW) ? i : 0);
	}
	digitalWrite(this->ssPin, HIGH);
	return response;
}