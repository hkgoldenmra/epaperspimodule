#ifndef EPAPERSPIMODULE_H
#define EPAPERSPIMODULE_H

#include "SPIModule.h"

class EPaperSPIModule {
	private:
		SPIModule spiModule;
		byte dcPin;
		byte rstPin;
		byte busyPin;
		unsigned int width;
		unsigned int height;
		void declare(byte, byte, byte, byte, byte, byte, unsigned int, unsigned int);
		void waitUntilIdle();
	public:
		EPaperSPIModule(byte, byte, byte, unsigned int, unsigned int);
		EPaperSPIModule(byte, byte, byte, byte, byte, byte, unsigned int, unsigned int);
		void initial();
		void reset();
		void sendCommand(byte);
		void sendData(byte);
		void clearScreen();
		void panelSetting();
		void powerOff();
		void powerOn();
		void boosterSoftStart();
		void deepSleep();
		void dataStartTransmission1();
		void displayRefresh();
		void dataStartTransmission2();
		void vcomDataIntervalSetting();
		void partialWindow(unsigned int, unsigned int, unsigned int, unsigned int);
		void partialIn();
		void partialOut();
		void displayScreen();
		void terminate();
		void drawTile(byte[], unsigned int, unsigned int, unsigned int, unsigned int);
		void drawTile(byte[], unsigned int, unsigned int, unsigned int, unsigned int, bool);
};

#endif