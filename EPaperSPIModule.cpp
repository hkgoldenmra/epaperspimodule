#include "EPaperSPIModule.h"

void EPaperSPIModule::declare(byte dinPin, byte clkPin, byte csPin, byte dcPin, byte rstPin, byte busyPin, unsigned int width, unsigned int height) {
	this->spiModule = SPIModule(csPin, dinPin, MISO, clkPin);
	this->dcPin = dcPin;
	this->rstPin = rstPin;
	this->busyPin = busyPin;
	this->width = ((width % 8 > 0) ? width + width % 8 : width);
	this->height = height;
}

void EPaperSPIModule::waitUntilIdle() {
	// LOW: busy
	// HIGH: idle
	while (digitalRead(this->busyPin) == LOW) {
		// do something while waiting if required
	}
}

EPaperSPIModule::EPaperSPIModule(byte dcPin, byte rstPin, byte busyPin, unsigned int width, unsigned int height) {
	this->declare(MOSI, SCK, SS, dcPin, rstPin, busyPin, width, height);
}

EPaperSPIModule::EPaperSPIModule(byte dinPin, byte clkPin, byte csPin, byte dcPin, byte rstPin, byte busyPin, unsigned int width, unsigned int height) {
	this->declare(dinPin, clkPin, csPin, dcPin, rstPin, busyPin, width, height);
}

void EPaperSPIModule::initial() {
	this->spiModule.initial();
	pinMode(this->dcPin, OUTPUT);
	pinMode(this->rstPin, OUTPUT);
	digitalWrite(this->rstPin, HIGH);
	pinMode(this->busyPin, INPUT);
	// E-Paper initial
	this->reset();
	this->boosterSoftStart();
	this->powerOn();
	this->panelSetting();
}

void EPaperSPIModule::reset() {
	digitalWrite(this->rstPin, LOW);
	digitalWrite(this->rstPin, HIGH);
}

void EPaperSPIModule::sendCommand(byte data) {
	digitalWrite(this->dcPin, LOW);
	this->spiModule.transfer(data);
}

void EPaperSPIModule::sendData(byte data) {
	digitalWrite(this->dcPin, HIGH);
	this->spiModule.transfer(data);
}

void EPaperSPIModule::clearScreen() {
	const unsigned int LENGTH = this->width / 8 * this->height;
	this->dataStartTransmission1();
	for (unsigned int i = 0; i < LENGTH; i++) {
		this->sendData(0xFF);
	}
	this->dataStartTransmission2();
	for (unsigned int i = 0; i < LENGTH; i++) {
		this->sendData(0xFF);
	}
}

/**
 * Panel Setting
 */
void EPaperSPIModule::panelSetting() {
	this->sendCommand(0x00);
	this->sendData(0x0F);
}

/**
 * Power Off
 */
void EPaperSPIModule::powerOff() {
	this->sendCommand(0x02);
	this->waitUntilIdle();
}

/**
 * Power On
 */
void EPaperSPIModule::powerOn() {
	this->sendCommand(0x04);
	this->waitUntilIdle();
}

/**
 * Booster Soft Start
 */
void EPaperSPIModule::boosterSoftStart() {
	this->sendCommand(0x06);
	this->sendData(0x17);
	this->sendData(0x17);
	this->sendData(0x17);
}

/**
 * Deep Sleep
 */
void EPaperSPIModule::deepSleep() {
	this->sendCommand(0x07);
	this->sendData(0xA5);
}

/**
 * Data Start Transmission 1
 */
void EPaperSPIModule::dataStartTransmission1() {
	this->sendCommand(0x10);
}

/**
 * Display Refresh
 */
void EPaperSPIModule::displayRefresh() {
	this->sendCommand(0x12);
	this->waitUntilIdle();
}

/**
 * Data Start Transmission 2
 */
void EPaperSPIModule::dataStartTransmission2() {
	this->sendCommand(0x13);
}

/**
 * VCOM and Data Interval Setting
 */
void EPaperSPIModule::vcomDataIntervalSetting() {
	this->sendCommand(0x50);
	this->sendData(0xF7);
}

/**
 * Partial Window
 */
void EPaperSPIModule::partialWindow(unsigned int x, unsigned int y, unsigned int width, unsigned int height) {
	this->sendCommand(0x90);
	const unsigned int DX = (x & 0xF8) + width - 1;
	const unsigned int DY = y + height - 1;
	this->sendData(x >> 8);
	this->sendData(x & 0xF8);
	this->sendData(DX >> 8);
	this->sendData(DX | 0x07);
	this->sendData(y >> 8);
	this->sendData(y & 0xFF);
	this->sendData(DY >> 8);
	this->sendData(DY & 0xFF);
	this->sendData(0x01);
}

/**
 * Partial In
 */
void EPaperSPIModule::partialIn() {
	this->sendCommand(0x91);
}

/**
 * Partial Out
 */
void EPaperSPIModule::partialOut() {
	this->sendCommand(0x92);
}

void EPaperSPIModule::terminate() {
	this->vcomDataIntervalSetting();
	this->powerOff();
	this->deepSleep();
}

void EPaperSPIModule::drawTile(byte tiles[], unsigned int x, unsigned int y, unsigned int width, unsigned int height) {
	this->drawTile(tiles, x, y, width, height, false);
}

void EPaperSPIModule::drawTile(byte tiles[], unsigned int x, unsigned int y, unsigned int width, unsigned int height, bool reded) {
	this->partialIn();
	this->partialWindow(x, y, width, height);
	if (reded) {
		this->dataStartTransmission2();
	} else {
		this->dataStartTransmission1();
	}
	for (unsigned int i = 0; i < width / 8 * height; i++) {
		this->sendData(tiles[i]);
	}
	this->partialOut();
}