#include "EPaperSPIModule.h"

EPaperSPIModule epaper = EPaperSPIModule(7, 6, 5, 4, 3, 2, 400, 300);
byte blackBlock[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
unsigned int s = 8;

void setup() {
	Serial.begin(115200);
//	pinMode(9, OUTPUT);
//	digitalWrite(9, HIGH);
//	pinMode(8, OUTPUT);
//	digitalWrite(8, LOW);
	epaper.initial();
	epaper.clearScreen();
	// HELLO
	// WORLD
	bool reded = false;
	drawH(0, 0, reded);
	drawE(48, 0, reded);
	drawL(96, 0, reded);
	drawL(144, 0, reded);
	drawO(192, 0, reded);
	drawW(0, 64, reded);
	drawO(48, 64, reded);
	drawR(96, 64, reded);
	drawL(144, 64, reded);
	drawD(192, 64, reded);
	epaper.displayRefresh();
}

void loop() {
}

void drawH(unsigned int x, unsigned int y, bool reded) {
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 8, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 16, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 24, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
}

void drawE(unsigned int x, unsigned int y, bool reded) {
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 8, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 16, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 24, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 8, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 16, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 24, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 8, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 16, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 24, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
}

void drawL(unsigned int x, unsigned int y, bool reded) {
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 8, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 16, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 24, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
}

void drawO(unsigned int x, unsigned int y, bool reded) {
	epaper.drawTile(blackBlock, x + 8, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 16, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 24, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 8, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 16, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 24, y, s, s, reded);
}

void drawW(unsigned int x, unsigned int y, bool reded) {
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 16, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 16, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 8, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 24, y, s, s, reded);
}

void drawR(unsigned int x, unsigned int y, bool reded) {
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 8, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 16, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 24, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 8, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 16, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 24, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
}

void drawD(unsigned int x, unsigned int y, bool reded) {
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 8, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 16, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 24, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 32, y, s, s, reded);
	y += 8;
	epaper.drawTile(blackBlock, x + 0, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 8, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 16, y, s, s, reded);
	epaper.drawTile(blackBlock, x + 24, y, s, s, reded);
}